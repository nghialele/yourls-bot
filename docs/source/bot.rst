bot package
===========

.. toctree::

    bot.adduser
    bot.changeurl
    bot.changekeyword
    bot.constants
    bot.deleteshorturl
    bot.errorhandler
    bot.inline
    bot.kickuser
    bot.setup
    bot.simplecommands
    bot.utils